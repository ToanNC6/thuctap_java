package vn.com.fsoft.webbanhangonline.dao.impl;

import java.util.List;

import javax.transaction.Transactional;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;



import vn.com.fsoft.webbanhangonline.dao.UserDao;
import vn.com.fsoft.webbanhangonline.entity.User;
import vn.com.fsoft.webbanhangonline.model.Login;
import vn.com.fsoft.webbanhangonline.model.UserInfo;

@Transactional
public class UserDaoImpl implements UserDao{

	@Autowired
	private SessionFactory factory;
	
	@Override
	public boolean login(Login login) {
		
		Session session = factory.getCurrentSession();// lay phien lv hien tai
		
		 String sql = "Select new " + UserInfo.class.getName()//
	                + "(a.id, a.userName, a.passWord, a.name, a.ngaySinh, a.gioiTinh, a.diaChi)"//
	                + " from " + User.class.getName() + " a where UseName = :UseName and Pass= :Pass ";
		
		Query query= session.createQuery(sql);
		
		
		query.setParameter("UseName", login.getName());
		query.setParameter("Pass", login.getPass());
		
		
		if(query.list().size()==1)
			return true;
		return false;
	}
	
	
	@Override
	public List<String> getUser(String userName) {
        String sql = "Select r.user "//
                + " from " + User.class.getName() + " r where r.user.username = :username ";
 
        Session session = factory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("username", userName);
         
        List<String> roles = query.list();
 
        return roles;
    }
	
	
	@Override
	public List<UserInfo> listUserInfos() {
		Session session = factory.getCurrentSession();// lay phien lv hien tai
		
		 String sql = "Select new " + UserInfo.class.getName()//
	                + "(a.id, a.userName, a.passWord, a.name, a.ngaySinh, a.gioiTinh, a.diaChi)"//
	                + " from " + User.class.getName() + " a";
		
		Query query= session.createQuery(sql);
		return query.list();
	}
	

}
