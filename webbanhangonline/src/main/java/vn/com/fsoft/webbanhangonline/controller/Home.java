package vn.com.fsoft.webbanhangonline.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.com.fsoft.webbanhangonline.dao.UserDao;
import vn.com.fsoft.webbanhangonline.model.Login;

@Controller
public class Home {
	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping("/")
    public String hello(Model model) {
        
//        model.addAttribute("greeting", "Hello Spring MVC");
        
        return "index";
        
    }
	@RequestMapping("/login")
	public String login(HttpServletRequest request, Model model) {
		String name= request.getParameter("username");
		String pass= request.getParameter("password");
		Login login= new Login(name, pass);
		boolean kt=userDao.login(login);
		if(kt) {
			model.addAttribute("xinchao", name);
			
			return "info";
		}
		model.addAttribute("Loi", "Tai khoan kh dung");
		return "index";
		
		
	}
	@RequestMapping(value = "/info")
	   public String userInfo(Model model, Principal principal) {
	 
	  
	       // Sau khi user login thanh cong se co principal
	       String userName = principal.getName();
	 
	       System.out.println("User Name: "+ userName);
	 
	       return "info";
	   }
	
}
