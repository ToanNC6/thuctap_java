package vn.com.fsoft.webbanhangonline.dao;

import java.util.List;

import vn.com.fsoft.webbanhangonline.model.Login;
import vn.com.fsoft.webbanhangonline.model.UserInfo;

public interface UserDao {
		
	public boolean login(Login login);
	 public List<UserInfo> listUserInfos();
	public List<String> getUser(String userName);
	
}
