package Ngay2t7;

import java.util.ArrayList;
import java.util.List;

import DTHinhHoc.Main;

public class BBetweenTwoSets {
	static int gcd(int a, int b) {
	    if (b == 0) {
	        return a;
	    }
	    return gcd(b, a % b);
	}

	static int lcm(int a, int b) {

	    return (a * b) / gcd(a, b);
	}
	
	
	static int getTotalX(List<Integer> a, List<Integer> b) {

	     int num1 = a.get(0);
	    for (int i = 0; i < a.size(); i++) {
	     num1 = lcm(a.get(i), num1);
	    }

	    int num = b.get(0);
	    for (int i = 0; i < b.size(); i++) {
	            num = gcd(b.get(i), num);
	    }
	    int counter = 0;
	    for(int i=1; i<= num/num1; i++){
	        if(num % (num1*i)==0)
	            ++counter;

	            }


	return counter;
	}
	
	public static void main(String[] args) {
		List<Integer> a= new ArrayList<Integer>();
		List<Integer> b= new ArrayList<Integer>();
		a.add(2);
		a.add(4);
		b.add(16);
		b.add(64);
		b.add(96);
		System.out.println(getTotalX(a, b));
	}

}
